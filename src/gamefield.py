from globvars import *


class PlayField:
    def __init__(self):
        self.__plates_counter = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        self.__rowcounter = [0, 0, 0, 0, 0, 0, 0]
        self.__playground = []
        self.__plates = []

    def reset(self):
        self.__plates_counter = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        self.__rowcounter = [0, 0, 0, 0, 0, 0, 0]

        self.drawfield()

    def setrowcountervalue(self,x, value):
        self.__rowcounter[x] = value

    def setplatescountervalue(self,x,y,value):
        self.__plates_counter[x][y] = value

    def createcopy(self):
        gamefield = PlayField()
        gamefield.setplates_counter(self.__plates_counter)
        gamefield.setrowcounter(self.__rowcounter)
        return gamefield

    def setplates_counter(self, plates_counter):
        self.__plates_counter = plates_counter

    def setrowcounter(self, rowcounter):
        self.__rowcounter = rowcounter

    def settone(self, x, y, l, m,  value):
        self.__plates_counter[x][y] = value
        self.__rowcounter[l] += 1
        if value == -1:
            globvars.w.itemconfig(self.__plates[m - self.__rowcounter[l] + 1], fill="yellow")
        elif value == 1:
            globvars.w.itemconfig(self.__plates[m - self.__rowcounter[l] + 1], fill="red")
        else:
            globvars.w.itemconfig(self.__plates[m - self.__rowcounter[l] + 1], fill="white")

    def getrowcounter(self):
        return self.__rowcounter

    def getplatescounter(self):
        return self.__plates_counter

    def createfield(self):
        for i in range(0, 630 + 1, 90):
            self.__playground.append(globvars.w.create_rectangle(0, 0, i, 540, fill="blue"))
        for i in range(80, 700, 90):
            for j in range(80, 600, 90):
                self.__plates.append(globvars.w.create_oval(i, j, i - 70, j - 70, fill="white"))

    def drawfield(self):
        for i in range(0, 7):
            for j in range(0, 6):
                if -1 == self.__plates_counter[i][j]:
                    globvars.w.itemconfig(self.__plates[j + (6 * i)], fill="yellow")
                elif 1 == self.__plates_counter[i][j]:
                    globvars.w.itemconfig(self.__plates[j + (6 * i)], fill="red")
                else:
                    globvars.w.itemconfig(self.__plates[j + (6 * i)], fill="white")