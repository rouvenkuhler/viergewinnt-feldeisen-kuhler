from globvars import *

class checkwin:
    def winchecker(self):
        v = self.__checkvertical(self, plate, plate_id, rowvar, colvar)
        h = self.__checkhorizontal(self, plate, plate_id, rowvar, colvar)
        d_tl = self.__checkdiagonaltoright(self, plate, plate_id, rowvar, colvar)
        d_tr = self.__checkdiagonaltoleft(self, plate, plate_id, rowvar, colvar)

    def __checkvertical(self, plate, plate_id, rowvar, colvar):
        run = True
        while (colvar < 5) & run:
            if globvars.playfield.getplatescounter()[rowvar][colvar+1] == plate_id:
                plate += 1
                colvar += 1
            else:
                run = False
        return plate

        run = True
        while(colvar > 0) & run:
            if globvars.playfield.getplatescounter()[rowvar][colvar-1] == plate_id:
                colvar -= 1
            else:
                run = False

    def __checkhorizontal(self, plate, plate_id, rowvar, colvar):
        run = True
        while (rowvar < 6) & run:
            if globvars.playfield.getplatescounter()[rowvar + 1][colvar] == plate_id:
                plate += 1
                rowvar += 1
            else:
                run = False
        return plate

        run = True
        while (rowvar > 0) & run:
            if globvars.playfield.getplatescounter()[rowvar - 1][colvar] == plate_id:
                rowvar -= 1
            else:
                run = False

    def __checkdiagonaltoright(self, plate, plate_id, rowvar, colvar):
        run = True
        while (rowvar < 6) & (colvar < 5) & run:
            if globvars.playfield.getplatescounter()[rowvar+1][colvar+1] == plate_id:
                plate += 1
                rowvar += 1
                colvar += 1
            else:
                run = False
        return plate
        run = True
        while (rowvar > 0) & (colvar > 0) & run:
            if globvars.playfield.getplatescounter()[rowvar-1][colvar-1] == plate_id:
                rowvar -= 1
                colvar -= 1
            else:
                run = False



    def __checkdiagonaltoleft(self, plate, plate_id, rowvar, colvar):