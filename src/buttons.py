from enum import Enum
import tkinter as tk


class Button:
    def __init__(self, enumbutton, window):
        self.__window = window
        self.__retvalue = 0
        button = enumbutton
        self.__button = None
        if button == EnumButtons.buttonreset:
            self.__button = tk.Button(window, text='reset', fg="red", command=lambda: self.__setretvalue(EnumButtons.buttonreset))
        if button == EnumButtons.buttonCancel:
            self.__button = tk.Button(window, text='cancel', fg="red", command=lambda: self.__setretvalue(EnumButtons.buttonCancel))
        if button == EnumButtons.buttonOK:
            self.__button = tk.Button(window, text='OK', fg="green", command=lambda: self.__setretvalue(EnumButtons.buttonOK))
        if button == EnumButtons.buttonnonhuman:
            self.__button = tk.Button(window, text='AI', fg='blue4', command=lambda: self.__setretvalue(EnumButtons.buttonnonhuman))
        if button == EnumButtons.buttonhuman:
            self.__button = tk.Button(window, text='Player', fg='green', command=lambda: self.__setretvalue(EnumButtons.buttonhuman))
        if button == EnumButtons.butoneasy:
            self.__button = tk.Button(window, text='easy', fg='green', command=lambda: self.__setretvalue(button))
        if button  == EnumButtons.buttonmedium:
            self.__button = tk.Button(window, text='medium', fg='yellow', command=lambda: self.__setretvalue(button))
        if button == EnumButtons.buttonhard:
            self.__button = tk.Button(window, text='hard', fg='red', command=lambda: self.__setretvalue(button))

    def __setretvalue(self, retvalue):
        self.__window.destroy()
        self.__retvalue = retvalue

    def getretvalue(self):
        return self.__retvalue

    def setgrid(self, row, column, columnspan, sticky):
        self.__button.grid(row=row, column=column, columnspan=columnspan, sticky=sticky)


class EnumButtons(Enum):
    buttonreset = 0
    buttonCancel = 1
    buttonOK = 2
    buttonnonhuman = 3
    buttonhuman = 4
    butoneasy = 5
    buttonmedium = 6
    buttonhard = 7
    # ...
