from tkinter import *
from globvars import globvars


class App:
    def __init__(self, master):
        frame = Frame(master)
        frame.pack()

        topframe = Frame(master)
        topframe.pack(side=TOP)

        bottomframe = Frame(master)
        bottomframe.pack(side=BOTTOM)

        self.btnrestart = Button(topframe, text='reset', fg="red", command=self.restart)

        self.btnrestart.pack(side=TOP)

    def restart(self):
        try:
            globvars.t
        except NameError:
            print("Restart without winner")
        else:
            globvars.t.pack_forget()

        globvars.round_counter = 0

        globvars.playfield.reset()
