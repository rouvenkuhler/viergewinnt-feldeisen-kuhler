from tkinter import *

from globvars import globvars
from customDialog import *


def callback(event):
    gamemainloop(event.x)


def gamemainloop(x):
    for n in range(90, 630 + 1, 90):
        if (x <= n) & (x > (n - 90)):
            l = int(n / 90) - 1
            m = (int(n / 90) * 6) - 1
            x = int((m - globvars.playfield.getrowcounter()[l]) / 6)
            y = (m - globvars.playfield.getrowcounter()[l]) % 6
            status = False
            globvars.lastcoloumn = l
            if not globvars.playfield.getrowcounter()[l] >= 6:
                status = True
                globvars.round_counter += 1
                highscore = ((6 * 7) - globvars.round_counter) * 1000
                if globvars.round_counter % 2 == 0:
                    globvars.playfield.settone(x, y, l, m, -1)
                else:
                    globvars.playfield.settone(x, y, l, m, 1)
                break

    return status
