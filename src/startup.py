from app import App
from game import *
from gamefield import PlayField
from sys import platform


def startup():
    globvars.root = Tk()
    globvars.root.title('Viergewinnt')

    globvars.w = Canvas(globvars.root, width=630, height=540)
    globvars.w.pack()

    globvars.playfield = PlayField()
    globvars.playfield.createfield()

    if not globvars.isturnment:
        globvars.w.bind("<Button-1>", callback)
    globvars.t = Label(globvars.root, text='Dummy')

    globvars.appframe = App(globvars.root)
    secound = False
    if not globvars.isturnment:
        dialog = CD(globvars.root, EnumButtons.buttonhuman)
        dialog.setmessage('AI oder Spieler')
        if dialog.show() == EnumButtons.buttonnonhuman:
            globvars.useAI = True
            noai = True
            fs = CD(globvars.root, EnumButtons.buttonOK, EnumButtons.buttonCancel)
            fs.setmessage(secound)
            if fs.show() is not EnumButtons.buttonOK:
                secound = True
    else:
        globvars.AI = AI()

    if secound:
        globvars.AI.playround()

    if not globvars.isturnment:
        globvars.root.mainloop()


if __name__ == '__main__':
    startup()
