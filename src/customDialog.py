import tkinter as tk
from buttons import EnumButtons, Button
import globvars
from globvars import globvars


def raise_above_all(window):
    window.attributes('-topmost', 1)
    window.attributes('-topmost', 0)


class CD(tk.Toplevel):
    retvalue = tk.IntVar

    def __init__(self, master, *buttons, sticky="N"):
        tk.Toplevel.__init__(self, master)
        self.usebuttons = []
        self.sticky = sticky
        if len(buttons) == 0:
            self.__message = "Min One Button is requiered"
            self.usebuttons.append(Button(EnumButtons.buttonOK, self))
            return
        for button in buttons:
            self.usebuttons.append(Button(button, self))

    def setmessage(self, message):
        self.__message = message

    def show(self):
        __lable = tk.Label(self, text=self.__message)
        __lable.grid(row=0, column=0, columnspan=3, sticky="N")
        row = 1
        columspan = 1
        colum = 0
        if len(self.usebuttons) == 1:
            columspan = 3
        for button in self.usebuttons:
            if colum == 3:
                row += 1
                colum = 0

            button.setgrid(row, colum, columspan, self.sticky)
            colum += 1

        self.lift()
        self.grab_set()
        self.attributes("-topmost", True)
        self.wait_window()
        for button in self.usebuttons:
            if isinstance(button.getretvalue(), EnumButtons):
                return button.getretvalue()


