from startup import *


class Computerspieler:
    def __init__(self):
        self.__playernumber = 0
        self.__lastcoloumn = None
        self.__x = [90, 180, 270, 360, 450, 540, 630]
        self.AUTHOR = "Feldeisen & Kuhler"
        self.Name = "NAME"

    def start(self, playernr):
        self.__playernumber = playernr
        globvars.isturnment = True
        startup()

    def turn(self, column_last_player):
        first = False
        x = 0
        if column_last_player is not None:
            x = self.__x[column_last_player]
        else:
            first = True
        print(x)
        if not first:
            gamemainloop(x, True)
        self.setlastcoloumn()
        return self.__lastcoloumn

    def setlastcoloumn(self):
        self.__lastcoloumn = globvars.lastcoloumn
